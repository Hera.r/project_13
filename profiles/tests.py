from django.test import TestCase
from .models import Profile
from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse


class ProfilTest(TestCase):
    def setUp(self):
        self.c = Client()
        self.user = User.objects.create(
            username='luffy',
            email='op@gmail.com', password='top_secret'
        )
        self.profil = Profile.objects.create(user=self.user, favorite_city="Fuchsia")

    def test_index_profil(self):

        response = self.c.get(reverse('profiles_index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<title>Profiles</title>', response.content)

    def test_profil_retrieve(self):
        response = self.c.get(reverse('profile', args=[self.profil.user.username]))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<h1>luffy</h1>', response.content)
