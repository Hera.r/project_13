from django.contrib import admin
from .models import Profile
# Register your models here.
# from import_export.admin import ExportMixin
# from import_export.admin import ImportExportMixin

# @admin.register(Profile)
# class ProfileAdmin(ImportExportMixin, admin.ModelAdmin):
# 	list_display = ['user', 'favorite_city']


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'favorite_city']
