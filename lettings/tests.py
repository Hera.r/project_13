from django.test import TestCase
from .models import Letting, Address
from django.test import Client
from django.urls import reverse


class LettingsTest(TestCase):
    def setUp(self):
        self.c = Client()
        self.address = Address.objects.create(
            number=1023, street="Cité des fleurs", city="Wano",
            state="New World",
            zip_code=700, country_iso_code="OP-01"
        )
        self.letting = Letting.objects.create(title="Arc Wano", address=self.address)

    def test_letting(self):
        response = self.c.get(reverse('lettings_index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<title>Lettings</title>', response.content)

    def test_letting_retrieve(self):
        response = self.c.get(reverse('letting', args=[self.letting.id]))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<h1> Arc Wano </h1>', response.content)
