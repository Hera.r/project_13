from django.shortcuts import render
from .models import Letting

# Create your views here.


def lettings_index(request):
    lettings_list = Letting.objects.all()
    context = {'lettings_list': lettings_list}
    return render(request, 'lettings/lettings_index.html', context)


def letting(request, letting_id):
    letting = Letting.objects.get(id=letting_id)
    print(letting.title)
    context = {
        'title': letting.title,
        'address': letting.address,
    }
    return render(request, 'lettings/letting.html', context)
