FROM python:3

ENV PYTHONUBUFFERED 1

ENV PORT=8000

WORKDIR /app

COPY pyproject.toml /app

RUN pip install --upgrade pip

RUN pip3 install poetry

RUN poetry config virtualenvs.create false

RUN poetry install --no-dev

COPY . /app

RUN python3 manage.py collectstatic --noinput

CMD python3 manage.py runserver 0.0.0.0:$PORT
