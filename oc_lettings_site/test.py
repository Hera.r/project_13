from django.test import TestCase
from django.test import Client
from django.urls import reverse


class IndexTest(TestCase):

    def test_index(self):
        c = Client()
        response = c.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<title>Orange County Lettings</title>', response.content)
