from django.shortcuts import render


def index(request):
    return render(request, 'lettings/index.html')
